module Main where

--
import           App

-- base
import           Control.Monad
import           Data.List
import           Data.Maybe
import           System.Environment
import           System.Exit

-- warp
import           Network.Wai.Handler.Warp




-- |Handle only environment variables with favourite colours.
processColourDescr :: (String, String) -> Maybe (String, String)
processColourDescr (key, colour) =
  case stripPrefix "FAV_COLOUR_" key of
    Just name -> Just (name, colour)
    Nothing   -> Nothing


main :: IO ()
main = do
    userColours <- catMaybes . map processColourDescr <$> getEnvironment
    when (null userColours) $ do
      putStrLn "No input data!"
      exitFailure

    run 8080 (app userColours)
