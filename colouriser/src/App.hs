{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}

module App (ColourisedAPI(..), app) where

-- base
import           Data.List

-- servant-server
import           Servant

-- servant
import           Servant.API


type ColourisedAPI
  = Get '[JSON] String
  :<|> "favourites"
      :> Get '[JSON] [String]
  :<|> "favourite"
      :> Capture "name" String
      :> Get '[JSON] String



colourisedAPIHandler colours =
         rootHandler
    :<|> favourites
    :<|> favourite
  where
    rootHandler = return "Welcome to the mighty colour server!"
    favourites = return . nub . map snd $ colours
    favourite name =
      case lookup name colours of
        Just colour -> return colour
        Nothing     -> throwError (err400 { errBody = "No such user" })

app :: [(String, String)] -> Application
app userColours = serve (Proxy :: Proxy ColourisedAPI) (colourisedAPIHandler userColours)